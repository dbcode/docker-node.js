var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));

gulp.task('sass', () => gulp.src('dev/scss/styles.scss').pipe(sass()).pipe(gulp.dest('dev/css')));