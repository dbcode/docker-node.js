var http = require('http');
var server = http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Je suis un serveur node.js');
});
server.listen(8080);

console.log('Server running at http://localhost:8067/');