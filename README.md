# Container docker avec node 
## Mise en place du container

#### téléchargement fichiers

Aller dans le répertoire ou le projet va être téléchargé:

`cd myproject`

télécharger le repository Git:

`git clone https://gitlab.com/dbcode/docker-node.git .`


#### Première execution 

`docker-compose up --build -d`

#### execution suivantes

`docker-compose up -d`

#### accès console containeur

`docker exec -i -t myproject_web_1 bash`

adapter la partie `myproject_web_1` avec le nom du container sur votre station.

#### commandes utiles pour la console dans le containeur

`npm install gulp -g`

`npm install gulp --save-dev`

`npm install sass --save-dev`

`npm install gulp-sass --save-dev`

`npm install gulp-uglify --save-dev`

`npx gulp -v` ou `gulp -v`

`npx gulp sass` ou `gulp sass`


## Structure 

Pour modifier les parametres de bases des serveurs, il faut éditer le fichier **./docker-compose.yml**

Le dossier **./src** contient le repertoire du projet web


## Lien par défaut

http://localhost:8067

(Modifier le fichier server.js pour créer un serveur web qui repond à l'URL)


## Liste des commandes 

#### Démarrer les containers 

`docker-compose start`

#### Stopper les containers 

`docker-compose stop`

#### Supprimer les containers 

`docker-compose down -v`



## Docker-compose.yml

Ce fichier permet d'éditer certains des parametres des containers.

### Principaux paramètre

#### Ports

Permet d'éditer les ports utilisés par Docker. Ne modifiez que les premiers chiffre (par exemple **8067**:8080 en **8080**:8080) 

#### Volumes

Permet de faire un lien entre le systeme de fichier du container et de votre machine. Ne modifiez que la première valeur (Par exemple **./src**:/src en **./src/dev**:/src). Celle-ci doit correspondre a un répertoire exsitant contenant le projet Web.

### Tableau des paramètres

|  Nom          	| Exemple             	| Explication                                	|
|----------------:	|---------------------	|--------------------------------------------------------------------------------------	|
| **build**       	| context: ./       	| Paramètres de personnalisation techniques des container  	|
| **ports**       	| 8067:8080            	| Permet de définir les ports ouverts du container          |
| **volumes**     	| ./src:/src         	| Lien entre le système de fichier du container et de votre machine|